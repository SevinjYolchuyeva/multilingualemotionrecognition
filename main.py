#!/usr/bin/env python
# coding: utf-8

import argparse
from model_transformers.model_transformers import train, predict
from test_model.test_model import predict as testpredict
from textwrapper.textwrapper import BookLikeA
from os.path import join
tasks = ["multi_class","multi_label"]
models = ["bert", "albert", "xlm_roberta", "distilbert", "flaubert", "test","camembert","roberta_german","dbmdz_german","bert_german"]
methods = ["train", "predict"]

if __name__ == "__main__":
    # construct the argument parser and parse the arguments
    parser = argparse.ArgumentParser(description='Train and Evaluate model')
    parser.add_argument('--task', type=str, choices=tasks, required=True)
    parser.add_argument('--model', type=str, choices=models, required=True)
    parser.add_argument('--method', type=str, choices=methods, required=True)
    parser.add_argument('--out_path', type=str, help='path for output and cache', default=".")
    parser.add_argument('--train_data', type=str, help='(For training) path for training data', default=None)
    parser.add_argument('--test_data', type=str, help='(For training) path for testing data', default=None)
    parser.add_argument('--data_path', type=str, help='(For prediction) path to data', default=None)
    parser.add_argument('--out_name', type=str, help='(For prediction) output name', default=None)
    parser.add_argument('--gpu_id', type=int, help='GPU ID to use for single GPU training', default=3)
    # TODO: Generalise chapter_re
    parser.add_argument('--chapter_re', type=str, help='regex to recognise chapters', default=None)
    args = parser.parse_args()

    if args.method == "train":
        assert args.train_data is not None
        assert args.test_data is not None
        train(args.train_data, args.test_data, args.out_path, args.out_name, args.model, args.task, args.gpu_id)
    elif args.method == "predict":
        assert args.data_path is not None
        assert args.out_name is not None
        with open(args.data_path, 'r') as f:
            raw_text = f.read()
        # we really need raw_text to be preprocessed!
        text = BookLikeA(raw_text, args.chapter_re)
        if args.model == "test":
            output = testpredict(text.get_text_iter(), args.out_path, args.model, args.task)
        else:
            output = predict(text.get_text_iter(), args.out_path, args.model, args.task)
        text.annotate(output)
        text.save_to_file(join(args.out_path, args.out_name))
