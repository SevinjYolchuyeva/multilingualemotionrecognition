def train(train_data, test_data, out_path, out_name, model_type, task_type):
    pass

def predict(text, model_path, model_type, task_type):
    emotions = ["joy", "sadness", "fear", "anger", "disgust"]
    emo_code = [sum([ord(c) for c in emo]) for emo in emotions]
    def rate(s):
        text_val = sum([ord(c) for c in s])
        emos = [(text_val % x) / x for x in emo_code]
        return dict(zip(emotions, emos))
    return [rate(s) for s in text]