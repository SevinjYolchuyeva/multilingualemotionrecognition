from collections import OrderedDict
from tqdm import tqdm
from spacy.lang.en import English
import re
import json

flatten = lambda l: [item for sublist in l for item in sublist]

class BookLikeA():
    def __init__(self, raw_text, chapter_re):
        """
        Args:
            raw_text: str
            chapter_re: str - A regex for splitting by chapters, if None is supplied,
        then only split by sentences and paragraphs
        """
        # Sentences are a special case, use sentence segmentation for this.
        # Paragraphs seperated by "\n\n"
        # Chapters seperated by chapter_re
        cleaned_text = text_normalisation(raw_text) # see text_normalisation function definition
        if chapter_re is None:
            split_re = [r"\n\n", None]
            preserve_split_name = [False, False]
            level_names = ["paragraphs", "sentences"]
            rejoin_str = [".\n", " "]
        else:
            split_re = [chapter_re, r"\n\n", None]
            preserve_split_name = [True, False, False]
            level_names = ["chapters", "paragraphs", "sentences"]
            rejoin_str = [".\n", ".\n", " "] # strings to use for rejoining chapters, paragraphs, sentences
        nlp = English()
        nlp.add_pipe(nlp.create_pipe('sentencizer'))
        self.text_tree = TextTreeNode(cleaned_text, split_re, rejoin_str, preserve_split_name, level_names, nlp)   

    def get_text_iter(self):
        """Returns a list of texts for each node in the tree in a preorder"""
        text_nodes = self.text_tree.get_preorder()
        return [node.get_text() for node in text_nodes]

    def annotate(self, emotions):
        """Annotate the tree with emotions
        Args:
            emotions: Iter[Dict[string, float]]: emotions corresponding to each node in the tree (in a preorder)
        """
        text_leaves = self.text_tree.get_preorder()
        for leaf, emo in zip(text_leaves, emotions):
            leaf.emotion = emo

    def save_to_file(self, save_path):
        d = self.text_tree.to_dict()
        with open(save_path, "w") as f:
            json.dump(d, f, indent=4)

class TextTreeNode():
    def __init__(self, text, split_re, rejoin_str, preserve_split_name, level_names, nlp):
        """
        Args:
            text: str 
            split_re: List[str] - the regex for splitting at each level
            rejoin_str: List[str] - the string for rejoining at each level
            preserve_split_name: List[bool] - whether to preserve the match of split_re at each level
            level_names: List[str] - name of the children (plural) of each level
        The last 4 arguments should be lists of the same length.
        This length is the level of granularity you want to split the text into.
        """
        self.level_name = level_names[0]
        self.rejoin_str = rejoin_str[0]
        self.node_preserve_split_name = preserve_split_name[0]
        node_re, child_re = split_re[0], split_re[1:]
        node_preserve_split_name, child_preserve_split_name = preserve_split_name[0], preserve_split_name[1:]
        if node_re is not None:
            sub_texts = split_text(text, node_re, node_preserve_split_name, "UNNAMED")
        else:
            sub_texts = segment_sentences(text, nlp)
        self.emotion = None
        def subtext_to_child(v):
            if len(child_re) == 0:
                return TextLeaf(v)
            else:
                return TextTreeNode(v, child_re, rejoin_str[1:], child_preserve_split_name, level_names[1:], nlp)
        if node_preserve_split_name:
            self.child_nodes = OrderedDict([(k, subtext_to_child(v)) for k, v in sub_texts.items()])
        else:
            self.child_nodes = [subtext_to_child(v) for v in sub_texts]
    
    def get_preorder(self):
        if len(self.child_nodes) > 0 and (not isinstance(self.child_nodes, OrderedDict)) and isinstance(self.child_nodes[0], TextLeaf):
            return [self] + self.child_nodes
        elif isinstance(self.child_nodes, OrderedDict):
            x = [n.get_preorder() for n in self.child_nodes.values()]
            return [self] + flatten(x)
        else:
            x = [n.get_preorder() for n in self.child_nodes]
            return [self] + flatten(x)

    def to_dict(self):
        d = {}
        if self.emotion is not None:
            d["emotion"] = self.emotion
        if self.node_preserve_split_name:
            d[self.level_name] = OrderedDict([(k, v.to_dict()) for k, v in self.child_nodes.items()])
        else:
            d[self.level_name] = OrderedDict([(i, n.to_dict()) for i, n in enumerate(self.child_nodes)])
        return d

    def get_text(self):
        children = list(self.child_nodes.values()) if self.node_preserve_split_name else self.child_nodes
        children_texts = [n.get_text() for n in children]
        return self.rejoin_str.join(children_texts)

class TextLeaf(TextTreeNode):
    def __init__(self, text):
        self.text = text
        self.emotion = None

    def children(self):
        return None

    def to_dict(self):
        d = {}
        if self.emotion is not None:
            d["emotion"] = self.emotion
        d["text"] = self.text
        return d

    def get_text(self):
        return self.text

def split_text(text, node_re, node_preserve_split_name, default):
    """Split a text using a regex, and optionally preserve the matches that split the text
    Args:
        text: str
        node_re: str - regex for splitting
        node_preserve_split_name: bool - preserve the name matches that split the text, and use them as keys in a dict
        default: str - if node_preserve_split_name, set the key for the text before the first match of node_re
    Returns:
        Either a list of sub_texts or a dictionary (List[str] or OrderedDict[str])
    """
    sub_texts = [m.strip() for m in re.split(node_re, text)]
    if node_preserve_split_name:
        titles = [m.strip() for m in re.findall(node_re, text)]
        if len(sub_texts) > len(titles):
            titles = [default]+titles
        assert len(sub_texts) == len(titles)
        titles_numbered = ["{}_{}".format(str(i).zfill(3), title) for i, title in enumerate(titles)]
        return OrderedDict(zip(titles_numbered, sub_texts))
    else:
        return sub_texts

def segment_sentences(text, nlp):
    # using sentence segmentation, credit: Sevinj
    doc = nlp(text)
    sentences = [sent.string.strip() for sent in doc.sents]
    return sentences

def text_normalisation(raw_text):
    # There is lots of room for improvement here
    text = re.sub('\n+', '\n\n', raw_text)
    return text