# -*- coding:latin1 -*-
import torch
import numpy as np
import pandas as pd
import os
from simpletransformers.classification import ClassificationModel, MultiLabelClassificationModel
from sklearn.metrics import classification_report, accuracy_score, hamming_loss, precision_score, recall_score, f1_score
from sklearn.utils import class_weight
from sklearn import preprocessing
np.set_printoptions(precision=6, suppress=True, formatter={
                    "float_kind": "{:f}".format})


model_types = {
    'bert': {'model_type': 'bert', 'model_name': 'bert-base-cased'},
    'albert': {'model_type': 'albert', 'model_name': 'albert-base-v2'},
    'distilbert': {'model_type': 'distilbert', 'model_name': 'distilbert-base-cased'},
    'xlm_roberta': {'model_type': 'xlmroberta', 'model_name': 'xlm-roberta-base'},
    'flaubert': {'model_type': 'flaubert', 'model_name': 'flaubert/flaubert_base_cased'}, #french
    'camembert': {'model_type': 'camembert', 'model_name': 'camembert/camembert-base'},  #french
    'bert_german': {'model_type': 'bert', 'model_name': 'bert-base-german-cased'},
    'dbmdz_german': {'model_type':'bert', 'model_name':'bert-base-german-dbmdz-cased'},
    'roberta_german': {'model_type':'roberta', 'model_name':'uklfr/gottbert-base'}
    }

model_args = {'cuda': torch.cuda.is_available(),
              'threshold': 0.39,
              'class_names': ['anger', 'disgust', 'fear', 'happy', 'love', 'sad', 'surprise']}

train_args = {
    'n_gpu': 1,
    'num_train_epochs': 5,
    "train_batch_size": 10,
    "eval_batch_size": 10,
    "learning_rate":1e-5,
    "max_seq_length": 256,
    "evaluate_during_training": True,
    "evaluate_during_training_steps": 5000,
    "evaluate_during_training_verbose": True,
    "use_multiprocessing": False,
    "use_multiprocessing_for_evaluation":False,
    "fp16": False,
    "save_steps": -1,
    "use_early_stopping": False,
    #"early_stopping_patience": 3,
    "save_eval_checkpoints": False,
    "save_model_every_epoch": False,
    "reprocess_input_data": True,
    "overwrite_output_dir": True,
      }


def reading_data_ml(train_data, test_data):
    """
    :param train_data: str - path to the training data
    :param test_data:  str - path to the test data
    :return:
    """
    train_df = pd.read_csv(train_data)
    test_df = pd.read_csv(test_data)
    train_df["labels"] = train_df["labels"].apply(lambda x: str(x).strip('[]'))
    test_df["labels"] = test_df["labels"].apply(lambda x: str(x).strip('[]'))
    train_df["labels"] = train_df["labels"].apply(lambda x: list(map(int, x.split(','))))
    test_df["labels"] = test_df["labels"].apply(lambda x: list(map(int, x.split(','))))
    print(train_df.head())
    train_df.dropna(axis=0, how='any', inplace=True)
    return train_df, test_df
        

def reading_data_mc(train_data, test_data):
    """
    :param train_data: str - path to the training data
    :param test_data:  str - path to the test data
    :return:
    """
    train_df = pd.read_csv(train_data)
    test_df = pd.read_csv(test_data)
    train_df.dropna(axis=0, how='any', inplace=True)
    test_df.dropna(axis=0, how='any', inplace=True)
    le = preprocessing.LabelEncoder()
    train_df["labels"] = le.fit_transform(train_df["labels"])
    test_df["labels"] = le.fit_transform(test_df["labels"])
    class_names = list(le.classes_)
    print('class_names', class_names)
    return train_df, test_df, class_names


def sigmoid(z):
    """
    It always returns a value between 0 and 1 which is the probability of a thing.
    """
    return 1 / (1 + np.exp(-z))

def predict(text, model_path, model_type, task_type):
    """
    :param text: List[str] - list of texts
    :param model_path: str - path to the model folder
    :param model_type: str - bert, albert, xlm_roberta, distilbert
    :param task_type: str - 
    :return: None
    """
    # update model_args dict
    model_args.update(model_types[model_type])
    class_names = model_args['class_names']
    if task_type == "multi_label":
        model = MultiLabelClassificationModel(model_args['model_type'], model_name=model_path, args=train_args)
        _, raw_outputs = model.predict(text)
        probabilities = raw_outputs
    else:
        model = ClassificationModel(model_args['model_type'], model_name=model_path, args=train_args)
        _, raw_outputs = model.predict(text)
        probabilities = [sigmoid(i) for i in raw_outputs]
    output = [dict(zip(class_names, x)) for x in probabilities]
    return output

def train(train_data, test_data, out_path, out_name, model_type, task_type, gpu_id):
    """
    :param train_data: str - path to the training data
    :param test_data: str - path to the test data
    :param out_path: str - path to output
    :param out_name: str - the name of the output
    :param model_type:  str - bert, albert, xlm_roberta, distilbert
    :return: None
    """
    # update model_args dict
    single_gpu_id = gpu_id if (0 <= gpu_id < torch.cuda.device_count()) else 0
    model_args.update(model_types[model_type])
    if task_type == "multi_label":
        class_names = model_args['class_names']
        df_train, df_test = reading_data_ml(train_data, test_data)
    else:
        df_train, df_test, class_names = reading_data_mc(train_data, test_data)
    print('The number of training samples: {0}'.format(len(df_train)))
    print('The number of test samples: {0}'.format(len(df_test)))
    print('Emotions: {0}'.format(class_names))

    # set cwd for training output
    os.chdir(out_path)
    newpath = [out_path + 'outputs', out_path + 'cache_dir', out_path + 'outputs/best_model']
    for path in newpath:
        if not os.path.exists(path):
            os.makedirs(path)
    train_args['output_dir'] = out_path + 'outputs'
    train_args['cache_dir'] = out_path + 'cache_dir'
    train_args['best_model_dir'] = out_path + 'outputs/best_model'
    if task_type == "multi_label":
        # pos_weight can change by depending on data
        del train_args['n_gpu']
        
        model = MultiLabelClassificationModel(model_args['model_type'], model_args['model_name'],
                                              num_labels=len(class_names),
                                              use_cuda=model_args['cuda'], args=train_args,
                                              pos_weight=[1,5,20,1,1,1,1], #it can be change
                                              cuda_device=single_gpu_id)
        
    else:
        class_weights = class_weight.compute_class_weight('balanced', classes=np.unique(df_train['labels']),
                                                          y=df_train['labels'])
        if train_args['n_gpu'] > 1:
            model = ClassificationModel(model_args['model_type'], model_args['model_name'], num_labels=len(class_names),
                                        use_cuda=True, args=train_args,cuda_device=single_gpu_id)
        else:
            model = ClassificationModel(model_args['model_type'], model_args['model_name'], num_labels=len(class_names),
                                        use_cuda=model_args['cuda'], args=train_args, weight=list(class_weights))

    model.train_model(df_train, eval_df=df_test, show_running_loss=False)
    result, _, _ = model.eval_model(df_test)
    print('Result: ', result)
    predictions, raw_outputs = model.predict(df_test['text'].to_list())
    raw_outputs=raw_outputs/raw_outputs.sum(axis=1,keepdims=1)

    if task_type == "multi_class":
        reference = df_test['labels']
        print('Classification Report')
        print(classification_report(reference, predictions, target_names=class_names))
        print('Finish')
        return

    df_output = pd.DataFrame(raw_outputs, columns=class_names)

    df_output.to_csv(out_name + '.csv', index=None)
    predictions = df_output[class_names].values
    y_pred = (predictions >= model_args['threshold']).astype(int)
    y_true = np.array(df_test['labels'].values.tolist())
    print('Exact Match Ratio: {0}'.format(accuracy_score(y_true, y_pred, normalize=True, sample_weight=None)))
    print('Hamming loss: {0}'.format(hamming_loss(y_true, y_pred)))
    print('Recall: {0}'.format(precision_score(y_true=y_true, y_pred=y_pred, average='macro')))
    print('Precision: {0}'.format(recall_score(y_true=y_true, y_pred=y_pred, average='macro')))
    print('F1 Measure: {0}'.format(f1_score(y_true=y_true, y_pred=y_pred, average='macro')))
    print('Finish')